package com.example.hellotoast

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private var mCount = 0
    private lateinit var mShowCount: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mShowCount = findViewById(R.id.textView)
    }

    fun showToast(view: View) {
        Toast.makeText(this, R.string.toast_mesage, Toast.LENGTH_SHORT).show()
    }

    fun countUp(view: View) {
        mCount++;
        if(mShowCount != null){
            mShowCount.setText(Integer.toString(mCount))
        }
    }
}