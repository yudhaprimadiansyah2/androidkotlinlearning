package com.latihan.composequadrant

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.latihan.composequadrant.ui.theme.ComposeQuadrantTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeQuadrantTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    QuadrantCompose()
                }
            }
        }
    }
}

@Composable
fun QuadrantCompose() {
    Column(Modifier.fillMaxWidth()) {
        Row(Modifier.weight(1f)) {
            QuadrantCard("Text Composable", "Displays text and follows Material Design guidelines.", Color.Green ,modifier = Modifier.weight(1f))
            QuadrantCard("Image composable","Creates a composable that lays out and draws a given Painter class object.", Color.Yellow,modifier = Modifier.weight(1f))
        }
        Row(Modifier.weight(1f)) {
            QuadrantCard("Row composable", "A layout composable that places its children in a horizontal sequence.", Color.Cyan,modifier = Modifier.weight(1f))
            QuadrantCard("Column composable", "A layout composable that places its children in a vertical sequence.", Color.LightGray,modifier = Modifier.weight(1f))
        }
    }
}

@Composable
fun QuadrantCard(
    title: String,
    text: String,
    backgroundColor: Color,
    modifier: Modifier = Modifier){
    Column(modifier = modifier
        .fillMaxSize()
        .background(backgroundColor)
        .padding(16.dp) ,verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally) {
        Text(title, modifier = Modifier.padding(10.dp), textAlign = TextAlign.Center, fontWeight = FontWeight.Bold)
        Text(text, modifier = Modifier
            .padding(top = 10.dp)
            .fillMaxWidth(), textAlign = TextAlign.Justify)
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ComposeQuadrantTheme {
        QuadrantCompose()
    }
}