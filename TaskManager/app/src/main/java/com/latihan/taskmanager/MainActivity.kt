package com.latihan.taskmanager

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.latihan.taskmanager.ui.theme.TaskManagerTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TaskManagerTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    TaskCompleted()
                }
            }
        }
    }
}

@Composable
fun TaskCompleted(){
    val gambar = painterResource(R.drawable.ic_task_completed)
    Column(horizontalAlignment = Alignment.CenterHorizontally, modifier = Modifier.fillMaxSize() ,verticalArrangement = Arrangement.Center) {
        Image(painter = gambar, contentDescription = null, Modifier.size(150.dp))
        Text(
            "All tasks Completed",
            modifier = Modifier
                .padding(bottom = 8.dp, top = 24.dp),
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Bold,
        )
        Text("Nice Works",
            textAlign = TextAlign.Center,
            fontSize = 16.sp
        )
    }

}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    TaskManagerTheme {
        TaskCompleted()
    }
}